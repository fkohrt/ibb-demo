---
widget: slider
headless: true  # This file represents a page section.
weight: 1
# ... Put Your Section Options Here (section position etc.) ...

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval: false

# Minimum slide height.
# Specify a height to ensure a consistent height for each slide.
height: 600px
font_size: S

item:
  - title: 2021 IBB World Championship
    content: 'The 2021 IBB World Championship will be held virtually. The competition dates will be announced at a later date.'
    # Choose `center`, `left`, or `right` alignment.
    align: left
    # Overlay a color or image (optional).
    #   Deactivate an option by commenting out the line, prefixing it with `#`.
    overlay_img: WJE-Offices-San-Diego-1770x800.jpg  # Image path relative to your `assets/media/` folder
    overlay_filter: 1  # Darken the image. Value in range 0-1.
  - title: 2019 IBB World Championship
    content: 'The 2019 Brain Bee World Championship was held in conjunction with the 10th IBRO World Congress of Neuroscience September 19-23, 2019 in Daegu, Korea. Twenty-eight national and regional champions traveled from all six continents to participate, with Ms. Yidou Weng from China taking home the Championship trophy.'
    align: left
    overlay_img: IMG_20190922_203817_by_Andrii_kzZZ5lv_IGarl56.jpg
    overlay_filter: 1
    cta_label: Learn more
    cta_url: 'https://example.org/'
  - title: 2019 Champions
    content: 'The winners of the 2019 International Brain Bee have been announced! We congratulate the National Champions, representing 28 different countries, for their participation and contribution to a wonderful IBB program.'
    align: left
    overlay_color: '#333'
    overlay_img: '2019_IBB_Awards_Presentation_Group_Photo_2.jpg'
    overlay_filter: 1
    cta_label: Meet the winners
    cta_url: 'https://example.org/'
---

