---
widget: featurette
headless: true  # This file represents a page section.
weight: 5
# ... Put Your Section Options Here (title etc.) ...

# Showcase personal skills or business features.
# Add/remove as many `feature` blocks below as you like.
# For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
  - icon: history
    icon_pack: fas
    name: 20
    description: Years
  - icon: users
    icon_pack: fas
    name: 25,500+
    description: Competitors Anually
  - icon: globe
    icon_pack: fas
    name: 50+
    description: Represented Countries
---

# The Neuroscience Competition for Teens

The International Brain Bee was founded in 1998 by Dr. Norbert Myslinski with a mission to build better brains to fight brain disorders. Since its inception, the International Brain Bee has inspired thousands of students to study and pursue careers in neuroscience. The International Brain Bee promotes student engagement with neuroscience through a three-tiered competition: students start by signing up for a Local Brain Bee, the winners of which compete in their country's National Brain Bee. Every year, National Brain Bees send one representative to compete at the International Brain Bee world championship.
